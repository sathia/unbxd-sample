$(document).ready(function(){
  $("a#upload").click(function(){
    url = location.pathname.replace("install", "upload");
    confirmation = confirm("Are you sure to upload data? It will take some time to upload.");
    if(confirmation){
      $.ajax({
        url: url,
        beforeSend: function(){
          $("span.loading").html("It would take some time to upload all products. Please wait for some time. Loading..");
        },
        success: function(data){
          $("span.loading").html("Thanks for your patience. products are uploaded");
        }
      });
    } else{
      $("span.loading").html("You did not uploaded products to server");
    }
    return false;
  });
  $(".product_desc ul li:odd").addClass("odd");
  $("#search_products").autocomplete({
    source: "search.php",
    minLength: 2,
    select: function( event, ui ) {
      //console.log(ui.item.id);
      location.href = location.pathname + "/show.php?id=" + ui.item.id;
    }
  });
});
