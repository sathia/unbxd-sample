<?php
  require('views/header.php');
  require('lib/product.php');
  $product = new Product();
  $url = $_SERVER["PHP_SELF"];
  $home_page = str_replace("/show.php", "", $url);
  $id = $_GET["id"];
  if(isset($id)){
    $product = $product->find_product_by_id($id);
  }
?>
<?php if(isset($product)): ?>
  <h3 class="title"><span class="name"><?= $product["name"] ?></span>
    <span> - <?= $product["category"] ?></span> <span>></span> <span><?= $product["sub_category"] ?></span>
    <a href="<?= $home_page ?>" class="common">Back to Home</a>
  </h3>
  <div class="product_desc">
    <ul>
      <li><p>Product type:</p><p><?= $product["type"] ?></p></li>
      <li><p>Model:</p><p><?= $product["sku"] ?></p></li>
      <li><p>Group:</p><p><?= $product["group_name"] ?></p></li>
      <li><p>Price:</p><p><?= $product["price"] ?></p></li>
      <li><p>Shipped in</p><p><?= $product["shipping_duration"] ?></p></li>
      <?php if(!empty($product["variants"])): ?>
        <li>
          <p>Related products:</p>
          <p>
            <?php foreach($product["variants"] as $variant_id => $variant): ?>
              <a href="<?= $url ?>?id=<?= $variant_id ?>"><?= $variant; ?></a>
            <?php endforeach; ?>
          </p>
        </li>
      <?php endif; ?>

    </ul>
  </div>
<?php else: ?>
  <p class="alert">Product not found</p>
<?php endif; ?>
<?php 
  require('views/footer.php');
?>

