<?php
class Db{
  public $con = null;
  public $db = 'products';
  public $table = null;
  public function __construct(){
    $this->con = mysqli_connect("127.0.0.1","root","password");
    if (mysqli_connect_errno()){
      echo "<p class='alert danger'>Failed to connect to MySQL: " . mysqli_connect_error() + '</p>';
      die();
    }
  }

  public function create_table(){
    $queries = array(
      "products" => "CREATE TABLE IF NOT EXISTS products(id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), store VARCHAR(255), name VARCHAR(255), category_id INT DEFAULT NULL, sub_category_id INT DEFAULT NULL, group_id INT DEFAULT NULL, sku VARCHAR(30), price FLOAT, shipping_duration INT);",
      "categories" => "CREATE TABLE IF NOT EXISTS categories(id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), name VARCHAR(255), type VARCHAR(255));",
      "groups" => "CREATE TABLE IF NOT EXISTS groups(id INT NOT NULL AUTO_INCREMENT, PRIMARY KEY(id), name VARCHAR(255));",
    );
    foreach($queries as $table => $query){
      if ($this->con->query($query)){
        echo "<p class='alert info'>Succesfully created table '$table'</p>";
      } else{
        echo "<p class='alert danger'>[Table Creation - '$table'] Something went wrong or Already exists</p>";
      }
    }
    echo "<p><a href='#' id='upload'>Upload products</a><span class='loading'></span></p>";
    return $this;
  }

  public function run_seeds(){
    $row = 1;
    if (($handle = fopen("public/movies-catalog.csv", "r")) !== FALSE) {
      echo "Storing data from CSV. It might take few seconds. Please wait.";
      while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
        $row++;
        if($row >= 8){
          $this->insert_products($data);
        }
      }
      fclose($handle);
    }
  }
  
  public function search_by($field, $value){
    $this->con->select_db($this->db);
    $sql = "SELECT id,name FROM $this->table WHERE $field LIKE '%$value%' GROUP BY group_id LIMIT 25;";
    $results = $this->con->query($sql);
    $json_array = array();
    $i = 0;
    foreach($results->fetch_all() as $product){
      $json_array[$i]['id'] = $product[0];
      $json_array[$i]['label'] = $product[1];
      $i+=1;
    }
    return json_encode($json_array);
  }

  private function insert_products($data){
    $this->con->select_db($this->db);
    $products = array();
    if($data[0] != ""){
      $this->table = "groups";
      $group_id = $this->find_or_create(array("name" => $data[0]));
      if($group_id!=null) $products['group_id'] = $group_id;
    }
    if($data[4] != ""){
      $this->table = "categories";
      $category_id = $this->find_or_create(array("name" => $data[4], "type" => "main"));
      if($category_id!=null) $products['category_id'] = $category_id;
    }
    if($data[5] != ""){
      $this->table = "categories";
      $sub_category_id = $this->find_or_create(array("name" => $data[5], "type" => "sub"));
      if($sub_category_id!=null) $products['sub_category_id'] = $sub_category_id;
    }
    $this->table = "products";
    $other_details = array("name" => $data[2], "store" => $data[3], "sku" => $data[1], "price" => $data[6], "shipping_duration" => $data[7]);
    $products = array_merge($products, $other_details);
    $product_id = $this->find_or_create($products);
    return $product_id;
  }

  private function find_or_create($fields=array()){
    $group = $this->first($fields);
    if($group == null){
      $group_id = $this->insert($fields);
    } else{
      $group_id = $group["id"];
    }
    return $group_id;
  }

  private function insert_category($name, $type){
    $table = "categories";
    $can_execute = $this->row_not_exists($table, "name = '$name' AND type = '$type'");
    $category_id = null;
    if($can_execute){
      $group_id = $this->insert("categories", array("name" => $name, "type" => $type));
    }
    return $category_id;
  }

  private function execute($sql){
    $result = $this->con->query($sql);
    return $this->con->insert_id;
  }
  
  public function first($fields=array(), $limit=1){
    $table = $this->table;
    $this->con->select_db($this->db);
    if($table == null){
      return false;
    }
    $query = $this->make_and_query($fields);
    $sql = "SELECT * FROM $this->table WHERE $query LIMIT $limit;";
    $result = $this->con->query($sql)->fetch_assoc();
    return $result;
  }

  public function make_and_query($fields){
    $query = "";
    $counter = 1;
    $total_fields = count($fields);
    if(!empty($fields)){
      foreach($fields as $key => $value){
        if($key=="id"){
          $query .= ($key . "=" . $value);
        } else{
          $query .= ($key . "=" . "'$value'");
        }
        if($total_fields != $counter){
          $query .= ' AND ';
        }
        $counter += 1;
      }
    }
    return $query;
  }

  public function insert($fields){
    $table = $this->table;
    if($table != null){
      $fields = "(" . implode(",", array_keys($fields)) . ") VALUES ('" . implode("','", array_values($fields)) . "');";
      $query = "INSERT INTO $table $fields";
      return $this->execute($query);
    }
    return false;
  }

  public function create_db(){
    $table_name = "products";
    $sql = "CREATE DATABASE IF NOT EXISTS $table_name;";
    if ($this->con->query($sql)){
      echo "<p class='alert info'>Succesfully created db '$table_name'</p>";
    } else{
      echo "<p class='alert danger'>[Database Creation] Something went wrong</p>";
    }
    $this->con->select_db($table_name);
    return $this;
  }
}
?>
