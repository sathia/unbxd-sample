<?php
  require('db.php');
  class Product extends Db{
    public $table = "products";
    public function find_by_name($name){
      return $this->search_by("name", $name);
    }
 
    public function get_variants($product_id, $group_id){
      $table = $this->table;
      $this->con->select_db($this->db);
      $variants = array();
      if($group_id==null){
        return $variants;
      } else{
        $sql = "SELECT id,name FROM products WHERE group_id=$group_id";
        $results = $this->con->query($sql)->fetch_all();
        foreach($results as $result){
          $variants[$result[0]] = $result[1];
        }
      }
      return $variants;
    }

    public function find_product_by_id($id){
      $table = $this->table;
      $this->con->select_db($this->db);
      $sql = "SELECT * FROM products LEFT OUTER JOIN groups ON products.group_id=groups.id INNER JOIN categories c1 ON c1.id=products.category_id INNER JOIN categories c2 ON c2.id=products.sub_category_id WHERE products.id=$id LIMIT 1;";
      $result = $this->con->query($sql)->fetch_all();
      $product = array();
      if(empty($result)){
        return $product;
      } else{
        $product["id"] = $result[0][0];
        $product["type"] = $result[0][1];
        $product["name"] = $result[0][2];
        $product["sku"] = $result[0][6];
        $product["price"] = $result[0][7];
        $product["shipping_duration"] = $result[0][8];
        $product["group_id"] = $result[0][9];
        $product["group_name"] = $result[0][10];
        $product["category"] = $result[0][12];  
        $product["sub_category"] = $result[0][15];
        $product["variants"] = $this->get_variants($product["id"], $product["group_id"]);
        return $product;
      }
    }

  }
?>
